﻿using UnityEngine;
using System.Collections;

//[System.Serializable]
//public class Boundary
//{
//    public float xMin, xMax, zMin, zMax;
//}

public class Player1_Movimentation : MonoBehaviour
{
    Rigidbody rb;
    //public Joystick joystick;

    //Variaveis dano
    int health = 3;

    //Variaveis velocidade
    private float moveHorizontal, moveVertical;
    public float moveSpeed;
    private bool isMoving;
    //public float tilt;
    //public Boundary boundary;

    //Variaveis Dodge
    float dodgeTime = 1f;
    private float dodgeTimer;
    private float dodgeSpeed;
    bool playerDodge;
    bool canDodge = true;

    private void Awake()
    {
        Application.targetFrameRate = 60;
        rb = GetComponent<Rigidbody>();
        Application.targetFrameRate = Screen.currentResolution.refreshRate;
    }

    //Seta valores de velocidade e dodge com inputs 
    private void Update()
    {
        moveHorizontal = Input.GetAxis("Horizontal");
        moveVertical = Input.GetAxis("Vertical");
        if (Input.GetButtonDown("Jump"))
        {
            playerDodge = true;
        }

        //#region Android
        ////comandos Android
        //if (joystick.Horizontal >= .2f)
        //{
        //    moveHorizontal = joystick.Horizontal;
        //}
        //else if (joystick.Horizontal <= -.2f)
        //{
        //    moveHorizontal = joystick.Horizontal;

        //}
        //if (joystick.Vertical >= .2f)
        //{
        //    moveVertical = joystick.Vertical;
        //}
        //else if (joystick.Vertical <= -.2f)
        //{
        //    moveVertical = joystick.Vertical;
        //}

        //#endregion
        //moveHorizontal = Input.GetAxisRaw("Horizontal");
        //moveVertical = Input.GetAxisRaw("Vertical");
    }

    //Realiza calculos de os inputs
    void WindowsMovement()
    {
        isMoving = (moveHorizontal != 0 || moveVertical != 0);
        if (isMoving)
        {
            var movement = new Vector3(moveHorizontal, moveVertical, 0);
            transform.position += movement * moveSpeed * Time.fixedDeltaTime;

            ////Logica de dodge
            if (playerDodge && canDodge)
            {
                dodgeSpeed = moveSpeed * (moveSpeed / moveSpeed + 0.5f);
                transform.position += (movement * dodgeSpeed) * moveSpeed * Time.fixedDeltaTime;
                playerDodge = false;
                dodgeSpeed = 0;
                canDodge = false;
            }
            //Logica de contador de tempo para nao ter dodge infinito
            else if (!canDodge)
            {
                dodgeTimer += Time.deltaTime;
                if (dodgeTimer > dodgeTime)
                {
                    canDodge = true;
                    dodgeTimer = 0;
                    playerDodge = false;
                }
            }
    //        rb.position = new Vector3
    //(
    //Mathf.Clamp(rb.position.x, boundary.xMin, boundary.xMax), 0f,
    //Mathf.Clamp(rb.position.y, boundary.zMin, boundary.zMax)
    //);

    //        rb.rotation = Quaternion.Euler(0.0f, 90f, rb.velocity.x * -tilt);

        }
    }

    private void FixedUpdate()
    {
        WindowsMovement();
    }

    public void Damage()
    {
        health--;
        if (health == 0)
        {
            Destroy(gameObject);
        }
    }
}
